#!/bin/bash

ln -fsv /usr/share/zoneinfo/Europe/Moscow /etc/localtime
timedatectl set-ntp true
hwclock --systohc
timedatectl status
