#!/bin/bash

# Define locales
sudo sed -i 's/# en_GB.UTF-8 UTF-8/en_GB.UTF-8 UTF-8/g' /etc/locale.gen
sudo sed -i 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/g' /etc/locale.gen
sudo sed -i 's/# ru_RU.UTF-8 UTF-8/ru_RU.UTF-8 UTF-8/g' /etc/locale.gen
sudo locale-gen

# Some useful aliases
alias install="sudo apt install -y"

# Programming
## Compilers and virtual machines
install chezscheme
install clang
install default-jdk
install default-jre
install g++
install gcc
install gfortran
install llvm
install perl
install python3
install racket
install racket-doc
install sbcl
install tcl
install tk

## Tools
install autoconf
install automake
install bear
install build-essential
install catch2
install ccache
install clang-format
install clang-tidy
install clangd
install cmake
install cmake-curses-gui
install cmake-format
install cmake-qt-gui
install cppcheck
install cppcheck-gui
install cpputest
install debuginfod
install doxygen
install expat
install exuberant-ctags
install gdb
install git
install graphicsmagick
install graphviz
install hotspot
install kcachegrind
install lcov
install libbenchmark-tools
install linux-perf
install lld
install lldb
install make
install meson
install mold
install musl
install musl-tools
install netcdf-bin
install ninja-build
install perl-tk
install poke
install pstoedit
install shellcheck
install strace
install universal-ctags
install valgrind
install valgrind-mpi

### IDE
install qtcreator
install qtcreator-data
install qtcreator-doc

## Development libraries
install cppzmq-dev
install fftw-dev
install freeglut3-dev
install glslang-dev
install libasio-dev
install libassimp-dev
install libbenchmark-dev
install libboost-all-dev
install libccfits-dev
install libcfitsio-dev
install libcgal-dev
install libcli11-dev
install libeigen3-dev
install libexpat1-dev
install libflatbuffers-dev
install libfmt-dev
install libfreeimage-dev
install libgd-dev
install libglew-dev
install libglm-dev
install libgmic-dev
install libgmp-dev
install libgsl-dev
install libgtest-dev
install libjansson-dev
install libjpeg-dev
install libjson-c-dev
install libjsoncpp-dev
install libjxl-dev
install libmpfr-dev
install libncurses-dev
install libnetcdf-dev
install libnetcdf-mpi-dev
install libopenal-dev
install libopenblas-dev
install libopenblas64-dev
install libopencv-dev
install libopengl-dev
install libopenmpi-dev
install libpng-dev
install libprotobuf-dev
install libpugixml-dev
install librange-v3-dev
install libraw-dev
install libspdlog-dev
install libsundials-dev
install libsuperlu-dev
install libxerces-c-dev
install libxext-dev
install libxft-dev
install libxml++2.6-dev
install libxml2-dev
install musl-dev
install ocl-icd-opencl-dev
install tcl-dev
install tk-dev
install vc-dev

### geant4
install libqt6opengl6-dev
install libvtk9-dev
install libxmu-dev
install qt6-3d-dev
install qt6-base-dev
install zlib1g-dev

### HDF
install libhdf5-dev
install libhdf5-mpi-dev
install libhdf5-mpich-dev
install libhdf5-openmpi-dev

### MPI
install mpi-default-bin
install mpi-default-dev
install mpich
install openmpi-bin
install openmpi-common
install openmpi-doc

### Mesa
install mesa-opencl-icd
install mesa-utils
install mesa-utils-bin
install mesa-va-drivers
install mesa-vdpau-drivers
install mesa-vulkan-drivers

### OpenCL
install opencl-c-headers
install opencl-clhpp-headers
install opencl-headers

### Vulkan
install vulkan-tools
install vulkan-validationlayers
install vulkan-validationlayers-dev

# Science
install gnuplot
install gnuplot-doc
install ngspice
install paraview

## Scientific Python
install jupyter
install jupyter-notebook
install python-matplotlib-data
install python3-matplotlib
install python3-nltk
install python3-numpy
install python3-pandas
install python3-pip
install python3-pypandoc
install python3-scipy
install python3-setuptools
install python3-sympy

## GNU Octave
install octave
install octave-dev
install octave-doc
mkdir -p "$HOME"/.octave
{
  echo "clc                    ;"
  echo "clear all              ;"
  echo "close all              ;"
  echo "format short           ;"
  echo "graphics_toolkit('qt') ;"
  echo "more off               ;"
  echo "pkg prefix ~/.octave   ;"
} > "$HOME"/.octaverc

## Maxima CAS
install maxima
install maxima-doc
install wxmaxima
install xmaxima

## Origin analogue
install labplot

# Editors
install ed
install emacs
install emacs-el
install emacs-goodies-el
install gawk
install grep
install groff
install nano
install ripgrep
install sed
install ugrep
install vim
install vim-doc

## Editor configuration
install editorconfig
install elpa-editorconfig
install vim-editorconfig

# System
install dbus-broker
install ffmpeg
install haveged
install plocate
install rsync
install smartmontools
install synaptic
install systemd-oomd

## Terminal utility
install bat
install bc
install cloc
install entr
install ghostscript
install htop
install hwinfo
install imagemagick
install lsof
install mc
install pandoc
install tealdeer

## System utility
install binutils
install coreutils
install dateutils
install diffutils
install elfutils
install findutils
install mailutils
install moreutils
install pax-utils
install pciutils
install plotutils
install psutils
install renameutils
install sysfsutils
install usbutils

## Archiving and compression
install 7zip
install arj
install atool
install bzip2
install clzip
install cpio
install gzip
install lhasa
install lrzip
install lz4
install lzip
install lzop
install minizip
install p7zip-full
install pigz
install tar
install unace
install unrar-free
install unzip
install xz-utils
install zip
install zstd

## Fonts
install fonts-anonymous-pro
install fonts-cantarell
install fonts-croscore
install fonts-crosextra-caladea
install fonts-crosextra-carlito
install fonts-dejavu
install fonts-droid-fallback
install fonts-fantasque-sans
install fonts-firacode
install fonts-font-awesome
install fonts-hermit
install fonts-jetbrains-mono
install fonts-junicode
install fonts-liberation
install fonts-monoid
install fonts-noto
install fonts-roboto
install fonts-terminus

## Office
install libreoffice
install libreoffice-help-en-us
install libreoffice-help-ru
install libreoffice-l10n-ru
install libreoffice-nlpsolver
install libreoffice-style-breeze
install libreoffice-style-colibre
install libreoffice-style-elementary
install libreoffice-style-karasa-jaga
install libreoffice-style-sifr
install libreoffice-style-sukapura

## Dictionary
install goldendict
install goldendict-wordnet

### Spell checker
install aspell
install aspell-en
install aspell-ru
install hunspell
install hunspell-en-gb
install hunspell-en-us
install hunspell-ru
install hyphen-en-gb
install hyphen-en-us
install hyphen-ru
install ispell
install mythes-en-us
install mythes-ru
install nuspell

## Latex
install texlive-full

### IDE for Latex
install texstudio
install texstudio-doc
install texstudio-l10n

## Gimp
install gimp
install gimp-data
install gimp-data-extras
install gimp-gmic
install gimp-help-en
install gimp-help-ru
install gmic

## Vector graphics
install inkscape

## Screenshots
install flameshot

## Video editing
install frei0r-plugins
install kdenlive
install mediainfo

## Calculator
install qalculate-gtk

## PDF and DjVu
install okular
install okular-extra-backends

## Man && Info
install info
install man-db

## Multimedia
install mpv
install vlc

## OBS Studio
install obs-plugins
install obs-studio

# Virtual machines
install            \
  qemu-guest-agent \
  qemu-system      \
  qemu-system-gui  \
  qemu-user        \
  qemu-utils

install bridge-utils
install libvirt-clients
install libvirt-daemon
install libvirt-daemon-system
install spice-client-gtk
install virt-manager
install virt-viewer
install virtinst

sudo systemctl enable libvirtd.service
sudo systemctl start  libvirtd.service

echo "options kvm_amd nested=1" | sudo tee /etc/modprobe.d/kvm.conf

sudo sed -i 's/#unix_sock_group/unix_sock_group/g'       /etc/libvirt/libvirtd.conf
sudo sed -i 's/#unix_sock_rw_perms/unix_sock_rw_perms/g' /etc/libvirt/libvirtd.conf

if [ "$(getent group libvirt)" ]
then
  echo "'libvirt' group exists!"
else
  sudo groupadd --system libvirt
fi

sudo usermod -aG disk         "$(whoami)"
sudo usermod -aG input        "$(whoami)"
sudo usermod -aG kvm          "$(whoami)"
sudo usermod -aG libvirt      "$(whoami)"
sudo usermod -aG libvirt-qemu "$(whoami)"

sudo systemctl restart libvirtd.service

sudo virsh net-autostart default
sudo virsh net-start     default

# System configuration
{
  echo "\$include /etc/inputrc"
  echo "set bell-style                        none"
  echo "set bind-tty-special-chars              on"
  echo "set blink-matching-paren                on"
  echo "set colored-stats                       on"
  echo "set completion-ignore-case              on"
  echo "set completion-prefix-display-length     5"
  echo "set completion-query-items              10"
  echo "set echo-control-characters            off"
  echo "set editing-mode                     emacs"
  echo "set enable-bracketed-paste              on"
  echo "set enable-keypad                       on"
  echo "set expand-tilde                        on"
  echo "set horizontal-scroll-mode              on"
  echo "set input-meta                          on"
  echo "set mark-directories                    on"
  echo "set mark-modified-lines                off"
  echo "set mark-symlinked-directories          on"
  echo "set match-hidden-files                  on"
  echo "set output-meta                         on"
  echo "set show-all-if-ambiguous               on"
  echo "set show-all-if-unmodified              on"
  echo "set visible-stats                       on"
} > "$HOME"/.inputrc

{
  echo "xrdb -merge .Xresources"
  echo "export DESKTOP_SESSION=plasma"
  echo "exec startplasma-x11"
} > "$HOME"/.xinitrc

{
  echo "Xft.antialias: true"
  echo "Xft.autohint:  false"
  echo "Xft.dpi:       96"
  echo "Xft.hinting:   true"
  echo "Xft.hintstyle: hintslight"
  echo "Xft.lcdfilter: lcddefault"
  echo "Xft.rgba:      rgb"
} > "$HOME"/.Xresources

echo "vm.dirty_background_ratio=5" | sudo tee    /etc/sysctl.d/99-dirty.conf
echo "vm.dirty_ratio=10"           | sudo tee -a /etc/sysctl.d/99-dirty.conf
echo "vm.swappiness=10"            | sudo tee    /etc/sysctl.d/99-swappiness.conf

# Enable system necessary services
sudo systemctl enable dbus-broker.service
sudo systemctl enable fstrim.timer
sudo systemctl enable haveged
sudo systemctl enable plocate-updatedb.timer
sudo systemctl enable smartmontools.service
sudo systemctl enable systemd-oomd.service
sudo systemctl enable systemd-timesyncd.service

sudo apt autoremove
