#!/bin/bash

sources_list="/etc/apt/sources.list"

sudo dd if=/dev/null of="$sources_list"

for repo_iso in *.iso
do
  [[ -e "$repo_iso" ]] || break

  repo_name="$(basename "$repo_iso" .iso)"
  if [ -d "$repo_name" ]
  then
    sudo rm -rf "$repo_name"
  fi
  7z x "$repo_iso" -o"$repo_name"

  path_to_repo="$(realpath "$repo_name")"
  release_name="$(ls --color=never "$path_to_repo"/dists)"

  component_list=""
  for component_name in "$path_to_repo"/dists/"$release_name"/*
  do
    if [ -d "$component_name" ]
    then
      component="$(basename "$component_name")"
      component_list+="$component "
    fi
  done
  component_list="$(echo "$component_list" | xargs | sort)"

  sudo chown -R _apt:root "$path_to_repo"
  # sudo chmod -R 700 "$path_to_repo"

  {
    echo "deb [trusted=yes] file:$path_to_repo $release_name $component_list"
    echo ""
  } | sudo tee -a "$sources_list"
done

sudo apt update && sudo apt upgrade
