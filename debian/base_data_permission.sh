#!/bin/bash

if [ -d /base ]
then
  sudo chown "$USER":"$USER" /base
fi

if [ -d /data ]
then
  sudo chown "$USER":"$USER" /data
fi
