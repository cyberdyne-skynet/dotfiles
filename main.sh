#!/bin/bash

pacman -Sy archlinux-keyring
pacman-key --init
pacman-key --populate archlinux
pacman -Sy
pacman -Sc

pacstrap /mnt               \
         amd-ucode          \
         base               \
         base-devel         \
         blas-openblas      \
         blas64-openblas    \
         bridge-utils       \
         dbus               \
         dbus-broker        \
         dbus-broker-units  \
         debugedit          \
         git                \
         git-lfs            \
         iptables-nft       \
         linux-firmware     \
         linux-lts          \
         linux-lts-docs     \
         linux-lts-headers  \
         man-db             \
         mkinitcpio         \
         posix              \
         sed                \
         sof-firmware       \
         sof-tools          \
         systemd            \
         systemd-libs       \
         systemd-resolvconf \
         systemd-sysvcompat \
         vi                 \
         wireless-regdb

genfstab -U /mnt >> /mnt/etc/fstab
arch-chroot /mnt
