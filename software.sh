#!/bin/bash

sudo pacman -S --needed \
     mariadb            \
     mariadb-clients    \
     mariadb-libs

sudo mariadb-install-db --user=mysql --basedir=/usr --datadir=/var/lib/mysql
sudo systemctl enable mariadb.service
sudo usermod -aG mysql "$(whoami)"

sudo pacman -S --needed postgresql

sudo pacman -S --needed sqlite

sudo pacman -S --needed lemon

sudo pacman -S --needed unixodbc

sudo pacman -S --needed freecad

sudo pacman -S --needed \
     gimp               \
     gimp-plugin-gmic

sudo pacman -S --needed inkscape

sudo pacman -S --needed \
     openttd            \
     openttd-opengfx    \
     openttd-opensfx

sudo pacman -S --needed wesnoth

sudo pacman -S --needed warzone2100

sudo pacman -S --needed widelands

sudo pacman -S --needed nmap

sudo pacman  -S --needed \
     wireshark-cli       \
     wireshark-qt

sudo usermod -aG wireshark "$(whoami)"

sudo pacman -S --needed \
     audacity           \
     audacity-docs

sudo pacman -S --needed ffmpeg

sudo pacman -S --needed kdenlive

sudo pacman -S --needed \
     mkvtoolnix-cli     \
     mkvtoolnix-gui

sudo pacman -S --needed mpv

sudo pacman -S --needed obs-studio

sudo pacman -S --needed \
     freeglut           \
     glew               \
     glm                \
     glslang            \
     glu                \
     libxext            \
     libxft             \
     opengl-man-pages

sudo pacman -S --needed rosegarden

sudo pacman -S --needed \
     fluidsynth         \
     soundfont-fluid

sudo pacman -S --needed qsynth

sudo pacman -S --needed lilypond

sudo pacman -S --needed vlc

sudo pacman -S --needed calibre

sudo pacman -S --needed flameshot

sudo pacman -S --needed ghostscript

sudo pacman -S --needed imagemagick

sudo pacman -S --needed keepassxc

sudo pacman -S --needed pandoc-cli

sudo pacman -S --needed telegram-desktop

sudo pacman -S --needed \
     texlive            \
     texlive-doc        \
     texlive-lang       \
     texlive-meta

sudo pacman -S --needed biber

sudo pacman -S --needed texlab

sudo pacman -S --needed texstudio

sudo pacman -S --needed adios2

sudo pacman -S --needed jemalloc

sudo pacman -S --needed mimalloc

sudo pacman -S --needed talloc

sudo pacman -S --needed arrow

sudo pacman -S --needed asio

sudo pacman -S --needed assimp

sudo pacman -S --needed autoconf

sudo pacman -S --needed automake

sudo pacman -S --needed boost

sudo pacman -S --needed botan

sudo pacman -S --needed \
     bcc                \
     bcc-tools          \
     bpf                \
     bpftrace           \
     python-bcc

sudo pacman -S --needed catch2

sudo pacman -S --needed ccache

sudo pacman -S --needed cli11

sudo pacman -S --needed \
     bison              \
     byacc              \
     flex

sudo pacman -S --needed cppcheck

sudo pacman -S --needed crypto++

sudo pacman -S --needed ctags

sudo pacman -S --needed doxygen

sudo pacman -S --needed fftw

sudo pacman -S --needed fio

sudo pacman -S --needed fmt

sudo pacman -S --needed \
     fpc                \
     fpc-src

sudo pacman -S --needed allegro

sudo pacman -S --needed ogre

sudo pacman -S --needed raylib

sudo pacman -S --needed \
     sdl2-compat        \
     sdl2_gfx           \
     sdl2_image         \
     sdl2_mixer         \
     sdl2_net           \
     sdl2_ttf           \
     sdl3               \
     sdl_gfx            \
     sdl_image          \
     sdl_mixer          \
     sdl_net            \
     sdl_sound          \
     sdl_ttf

sudo pacman -S --needed \
     csfml              \
     sfml

sudo pacman -S --needed geos

sudo pacman -S --needed \
     ghc                \
     ghc-libs           \
     ghc-static

sudo pacman -S --needed cabal-install

sudo pacman -S --needed \
     haskell-ghcide     \
     haskell-language-server

sudo pacman -S --needed stack

sudo pacman -S --needed gmic

sudo pacman -S --needed poke

sudo pacman -S --needed abseil-cpp

sudo pacman -S --needed benchmark

sudo pacman -S --needed flatbuffers

sudo pacman -S --needed google-glog

sudo pacman -S --needed gtest

sudo pacman -S --needed gperftools

sudo pacman -S --needed \
     protobuf           \
     protobuf-c

sudo pacman -S --needed graphviz

sudo pacman -S --needed \
     freeimage          \
     gd                 \
     jasper             \
     libjpeg-turbo      \
     libjxl             \
     libpng             \
     libraw             \
     libtiff            \
     libvips            \
     libwebp            \
     libwpg             \
     netpbm             \
     pstoedit

sudo pacman -S --needed graphicsmagick

sudo pacman -S --needed imath

sudo pacman -S --needed nodejs

sudo pacman -S --needed npm

sudo pacman -S --needed typescript

sudo pacman -S --needed typescript-language-server

sudo pacman -S --needed emscripten

sudo pacman -S --needed wabt

sudo pacman -S --needed wasmer

sudo pacman -S --needed wasmtime

sudo pacman -S --needed jansson

sudo pacman -S --needed jq

sudo pacman -S --needed json-c

sudo pacman -S --needed jsoncpp

sudo pacman -S --needed nlohmann-json

sudo pacman -S --needed libuv

sudo pacman -S --needed clojure

sudo pacman -S --needed leiningen

sudo pacman -S --needed racket

sudo pacman -S --needed guile

sudo pacman -S --needed sbcl

sudo pacman -S --needed lua

sudo pacman -S --needed fennel

sudo pacman -S --needed luacheck

sudo pacman -S --needed luajit

sudo pacman -S --needed lua-language-server

sudo pacman -S --needed luarocks

sudo pacman -S --needed libwebsockets

sudo pacman -S --needed meson

sudo pacman -S --needed microsoft-gsl

sudo pacman -S --needed nasm

sudo pacman -S --needed ncurses

sudo pacman -S --needed ninja

sudo pacman -S --needed notcurses

sudo pacman -S --needed openal

sudo pacman -S --needed opencascade

sudo pacman -S --needed vtk

sudo pacman -S --needed \
     opencv             \
     opencv-samples

sudo pacman -S --needed \
     openmpi            \
     openmpi-docs

sudo pacman -S --needed perf

sudo pacman -S --needed poco

sudo pacman -S --needed python

sudo pacman -S --needed \
     python-pip         \
     python-setuptools  \
     python-virtualenv

sudo pacman -S --needed range-v3

sudo pacman -S --needed oniguruma

sudo pacman -S --needed re2

sudo pacman -S --needed re2c

sudo pacman -S --needed shellcheck

sudo pacman -S --needed bash-language-server

sudo pacman -S --needed shellharden

sudo pacman -S --needed spdlog

sudo pacman -S --needed strace

sudo pacman -S --needed \
     tcl                \
     tk

sudo pacman -S --needed cmocka

sudo pacman -S --needed cppunit

sudo pacman -S --needed cpputest

sudo pacman -S --needed cunit

sudo pacman -S --needed utf8cpp

sudo pacman -S --needed valgrind

sudo pacman -S --needed kcachegrind

sudo pacman -S --needed vc

sudo pacman -S --needed \
     expat              \
     libxml++           \
     libxml2            \
     pugixml            \
     xerces-c

sudo pacman -S --needed \
     cppzmq             \
     zeromq

sudo pacman -S --needed asymptote

sudo pacman -S --needed \
     ccfits             \
     cfitsio

sudo pacman -S --needed eigen

sudo pacman -S --needed \
     gap                \
     gap-packages

sudo pacman -S --needed libsemigroups

sudo pacman -S --needed gmp

sudo pacman -S --needed cgal

sudo pacman -S --needed mpfr

sudo pacman -S --needed mpfi

sudo pacman -S --needed octave

# Add configuration file
mkdir -p "$HOME"/.octave
{
  echo "clc                    ;"
  echo "clear all              ;"
  echo "close all              ;"
  echo "format short           ;"
  echo "graphics_toolkit('qt') ;"
  echo "more off               ;"
  echo "pkg prefix ~/.octave   ;"
} > "$HOME"/.octaverc

sudo pacman -S --needed \
     gnuplot            \
     gnuplot-demos

sudo pacman -S --needed gsl

sudo pacman -S --needed igraph

sudo pacman -S --needed \
     maxima             \
     maxima-sbcl

sudo pacman -S --needed msolve

sudo pacman -S --needed     \
     netcdf-cxx             \
     netcdf-fortran-openmpi \
     netcdf-openmpi

sudo pacman -S --needed ngspice

sudo pacman -S --needed nlopt

sudo pacman -S --needed \
     pari               \
     pari-elldata       \
     pari-galdata       \
     pari-galpol        \
     pari-seadata       \
     pari-seadata-small

sudo pacman -S --needed singular

sudo pacman -S --needed sundials

sudo pacman -S --needed superlu

sudo pacman -S --needed z3

sudo pacman -S --needed \
     gnupg              \
     gpgme

sudo pacman -S --needed           \
     dmidecode                    \
     dnsmasq                      \
     gettext                      \
     libvirt                      \
     libvirt-storage-gluster      \
     libvirt-storage-iscsi-direct \
     open-iscsi                   \
     openbsd-netcat               \
     radvd                        \
     spice                        \
     swtpm

sudo systemctl start  libvirtd.service
sudo systemctl enable libvirtd.service

echo "options kvm_amd nested=1" | sudo tee /etc/modprobe.d/kvm.conf

sudo sed -i 's/#unix_sock_group/unix_sock_group/g'       /etc/libvirt/libvirtd.conf
sudo sed -i 's/#unix_sock_rw_perms/unix_sock_rw_perms/g' /etc/libvirt/libvirtd.conf

if [ "$(getent group libvirt)" ]
then
  echo "'libvirt' group exists!"
else
  sudo groupadd --system libvirt
fi

sudo usermod -aG kvm          "$(whoami)"
sudo usermod -aG libvirt      "$(whoami)"
sudo usermod -aG libvirt-qemu "$(whoami)"

sudo systemctl restart libvirtd.service

sudo virsh net-autostart default
sudo virsh net-start     default

sudo pacman -S --needed  \
     qemu-emulators-full \
     qemu-full           \
     qemu-guest-agent

sudo pacman -S --needed \
     libosinfo          \
     virt-manager       \
     x11-ssh-askpass

sudo pacman -S --needed virt-viewer

sudo pacman -S --needed freerdp

sudo pacman -S --needed geoip

sudo pacman -S --needed \
     geoip-database     \
     geoip-database-extra

sudo pacman -S --needed gnutls

sudo pacman -S --needed apache

sudo pacman -S --needed nginx

sudo pacman -S --needed \
     libssh             \
     openssh            \
     sshfs

sudo pacman -S --needed openssl

sudo pacman -S --needed openvpn

sudo pacman -S --needed \
     qbittorrent        \
     qbittorrent-nox

source ~/.dotfiles/aur.sh
