vim.api.nvim_set_keymap(
  "c", "Q", "q!",
  {
    silent = false,
  }
)

vim.api.nvim_set_keymap(
  "c", "W", "w!",
  {
    silent = false,
  }
)

vim.api.nvim_set_keymap(
  "i", "jj", "<ESC>",
  {
    noremap = true,
    silent  = true,
  }
)

vim.api.nvim_set_keymap(
  "n", "*", "*N",
  {
    noremap = true,
    silent  = true,
  }
)

vim.api.nvim_set_keymap(
  "n", "+", "<C-a>",
  {
    noremap = true,
    silent  = true,
  }
)

vim.api.nvim_set_keymap(
  "n", "-", "<C-x>",
  {
    noremap = true,
    silent  = true,
  }
)

vim.api.nvim_set_keymap(
  "n", "<C-a>", "gg<S-v>G",
  {
    silent  = true,
    noremap = true,
  }
)

vim.api.nvim_set_keymap(
  "n", "<C-h>", "<C-w>h",
  {
    noremap = true,
    silent  = true,
  }
)

vim.api.nvim_set_keymap(
  "n", "<C-j>", "<C-w>j",
  {
    noremap = true,
    silent  = true,
  }
)

vim.api.nvim_set_keymap(
  "n", "<C-k>", "<C-w>k",
  {
    noremap = true,
    silent  = true,
  }
)

vim.api.nvim_set_keymap(
  "n", "<C-l>", "<C-w>l",
  {
    noremap = true,
    silent  = true,
  }
)

vim.api.nvim_set_keymap(
  "n", "<LEADER>/", ":nohlsearch<CR>",
  {
    noremap = true,
    silent  = true,
  }
)

vim.api.nvim_set_keymap(
  "n", "<LEADER>O", "O<ESC>",
  {
    noremap = true,
    silent  = true,
  }
)

vim.api.nvim_set_keymap(
  "n", "<LEADER>bn", ":bnext<CR>",
  {
    noremap = true,
    silent  = true,
  }
)

vim.api.nvim_set_keymap(
  "n", "<LEADER>bp", ":bprevious<CR>",
  {
    noremap = true,
    silent  = true,
  }
)

vim.api.nvim_set_keymap(
  "n", "<LEADER>e", ":edit<SPACE>",
  {
    noremap = true,
    silent  = false,
  }
)

vim.api.nvim_set_keymap(
  "n", "<LEADER>n", ":tabnext<CR>",
  {
    noremap = true,
    silent  = true,
  }
)

vim.api.nvim_set_keymap(
  "n", "<LEADER>o", "o<ESC>",
  {
    noremap = true,
    silent  = true,
  }
)

vim.api.nvim_set_keymap(
  "n", "<LEADER>p", ":tabprevious<CR>",
  {
    noremap = true,
    silent  = true,
  }
)

vim.api.nvim_set_keymap(
  "n", "<LEADER>q", ":quit!<CR>",
  {
    noremap = true,
    silent  = true,
  }
)

vim.api.nvim_set_keymap(
  "n", "<LEADER>s", ":split<CR>",
  {
    noremap = true,
    silent  = true,
  }
)

vim.api.nvim_set_keymap(
  "n", "<LEADER>t", ":tabnew<SPACE>",
  {
    noremap = true,
    silent  = false,
  }
)

vim.api.nvim_set_keymap(
  "n", "<LEADER>v", ":vsplit<CR>",
  {
    noremap = true,
    silent  = true,
  }
)

vim.api.nvim_set_keymap(
  "n", "<LEADER>w", ":write!<CR>",
  {
    noremap = true,
    silent  = true,
  }
)

vim.api.nvim_set_keymap(
  "n", "N", "Nzz",
  {
    noremap = true,
    silent  = true,
  }
)

vim.api.nvim_set_keymap(
  "n", "n", "nzz",
  {
    noremap = true,
    silent  = true,
  }
)
