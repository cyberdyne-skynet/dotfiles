vim.api.nvim_create_autocmd(
  { "BufWritePre" },
  {
    pattern  = "*",
    callback = function(args)
      require("conform").format({ bufnr = args.buf })
      require("lint").try_lint()
    end,
    -- callback = function()
    --   vim.cmd([[%s/\s\+$//e]])
    --   vim.cmd([[lua vim.lsp.buf.format()]])
    --   require("lint").try_lint()
    -- end,
  }
)
