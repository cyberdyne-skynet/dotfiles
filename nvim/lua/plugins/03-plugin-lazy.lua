local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
  "ggandor/leap.nvim",
  "karb94/neoscroll.nvim",
  "kdheepak/tabline.nvim",
  "kylechui/nvim-surround",
  "lewis6991/gitsigns.nvim",
  "mfussenegger/nvim-lint",
  "numToStr/Comment.nvim",
  "nvim-lua/plenary.nvim",
  "nvim-lualine/lualine.nvim",
  "ray-x/go.nvim",
  "simrat39/rust-tools.nvim",
  "windwp/nvim-autopairs",

  {
    "catppuccin/nvim",
    "folke/tokyonight.nvim",
    "rebelot/kanagawa.nvim",
  },

  {
    "akinsho/bufferline.nvim",
    "akinsho/toggleterm.nvim",
  },

  {
    "stevearc/conform.nvim",
    "stevearc/oil.nvim",
  },

  {
    "L3MON4D3/LuaSnip",
    build = "make install_jsregexp",
  },

  {
    "nvim-tree/nvim-tree.lua",
    "nvim-tree/nvim-web-devicons",
  },

  {
    "nvim-treesitter/nvim-treesitter",
    build = ":TSUpdate",
  },

  {
    "nvim-treesitter/nvim-treesitter-textobjects",
    after    = "nvim-treesitter",
    requires = "nvim-treesitter/nvim-treesitter",
  },

  {
    "nvim-telescope/telescope-file-browser.nvim",
    "nvim-telescope/telescope.nvim",
    {
      "nvim-telescope/telescope-fzf-native.nvim",
      build = "make",
    },
  },

  {
    "hrsh7th/cmp-buffer",
    "hrsh7th/cmp-cmdline",
    "hrsh7th/cmp-nvim-lsp",
    "hrsh7th/cmp-nvim-lua",
    "hrsh7th/cmp-path",
    "hrsh7th/nvim-cmp",
    "saadparwaiz1/cmp_luasnip",
  },

  {
    "WhoIsSethDaniel/mason-tool-installer.nvim",
    "neovim/nvim-lspconfig",
    "nvimdev/lspsaga.nvim",
    {
      "williamboman/mason-lspconfig.nvim",
      "williamboman/mason.nvim",
    },
  },
})
