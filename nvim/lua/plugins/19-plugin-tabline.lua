require("tabline").setup({
  options = {
    show_devicons    = true,
    show_tabs_always = false,
  },
})

vim.api.nvim_set_keymap(
  "n", "<LEADER>n", ":TablineBufferNext<CR>",
  {
    noremap = true,
    silent  = true,
  }
)

vim.api.nvim_set_keymap(
  "n", "<LEADER>p", ":TablineBufferPrevious<CR>",
  {
    noremap = true,
    silent  = true,
  }
)

vim.api.nvim_set_keymap(
  "n", "<LEADER>t", ":TablineTabNew<SPACE>",
  {
    noremap = true,
    silent  = false,
  }
)
