require("nvim-treesitter.configs").setup({
  ensure_installed = {
    "bash",
    "c",
    "cmake",
    "cpp",
    "go",
    "javascript",
    "latex",
    "lua",
    "make",
    "markdown",
    "markdown_inline",
    "python",
    "rust",
  },
  auto_install     = true,
  highlight        = { enable = true },
  indent           = { enable = true },
  refactor         = { highlight_definitions = { enable = true } },
  sync_install     = false,
  textobjects      = {
    select = {
      enable                         = true,
      include_surrounding_whitespace = true,
      lookhead                       = true,
    },
  },
})
