require("rust-tools").setup({
  inlay_hints = {
    auto                 = true,
    only_current_line    = false,
    show_parameter_hints = true,
  },
})
