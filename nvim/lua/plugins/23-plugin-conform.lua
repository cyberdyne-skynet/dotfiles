require("conform").setup({
  format_on_save   = {
    lsp_format = "fallback",
  },

  formatters_by_ft = {
    ["*"] = {
      "codespell",
      "trim_whitespace",
    },
  },
})
