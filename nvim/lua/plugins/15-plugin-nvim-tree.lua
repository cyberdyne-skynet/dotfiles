require("nvim-tree").setup({
  open_on_tab = true,
  sort_by     = "case_sensitive",
  view        = {
    adaptive_size = true,
  },
})

vim.api.nvim_set_keymap(
  "n", "<F2>", ":NvimTreeToggle<CR>",
  {
    noremap = true,
    silent  = true,
  }
)
