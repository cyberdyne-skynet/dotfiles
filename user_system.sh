#!/bin/bash

while true
do
  clear
  echo "Chose the video card type:"

  echo ""
  echo "1. AMD"
  echo "2. INTEL"
  echo "3. NVIDIA"
  echo "4. VM (Virtual Machine)"
  echo ""

  read -r -p "VIDEO CARD TYPE: " user_choice

  case "$user_choice" in
    "1") video_card_type="AMD"    && break ;;
    "2") video_card_type="INTEL"  && break ;;
    "3") video_card_type="NVIDIA" && break ;;
    "4") video_card_type="VM"     && break ;;
    *  ) continue                          ;;
  esac
done
echo ""
echo "VIDEO CARD TYPE: $video_card_type" && sleep 5 && clear

sudo sed -i 's/#Color/Color/g'                         /etc/pacman.conf
sudo sed -i 's/#ParallelDownloads/ParallelDownloads/g' /etc/pacman.conf
sudo sed -i '/ParallelDownloads/a ILoveCandy'          /etc/pacman.conf

sudo pacman -S --needed          \
     adobe-source-code-pro-fonts \
     adobe-source-sans-fonts     \
     adobe-source-serif-fonts    \
     cantarell-fonts             \
     fontconfig                  \
     freetype2                   \
     freetype2-demos             \
     gnu-free-fonts              \
     harfbuzz                    \
     noto-fonts                  \
     noto-fonts-cjk              \
     noto-fonts-emoji            \
     noto-fonts-extra            \
     otf-cascadia-code           \
     otf-fantasque-sans-mono     \
     otf-font-awesome            \
     otf-hermit                  \
     otf-junicode                \
     otf-libertinus              \
     pango                       \
     terminus-font               \
     ttc-iosevka                 \
     ttf-anonymous-pro           \
     ttf-caladea                 \
     ttf-carlito                 \
     ttf-croscore                \
     ttf-dejavu                  \
     ttf-droid                   \
     ttf-fira-code               \
     ttf-jetbrains-mono          \
     ttf-liberation              \
     ttf-monoid                  \
     ttf-opensans                \
     ttf-roboto                  \
     ttf-roboto-mono

sudo pacman -S --needed            \
     otf-firamono-nerd             \
     otf-hermit-nerd               \
     ttf-cascadia-code-nerd        \
     ttf-cascadia-mono-nerd        \
     ttf-dejavu-nerd               \
     ttf-fantasque-nerd            \
     ttf-firacode-nerd             \
     ttf-hack-nerd                 \
     ttf-iosevka-nerd              \
     ttf-iosevkaterm-nerd          \
     ttf-iosevkatermslab-nerd      \
     ttf-jetbrains-mono-nerd       \
     ttf-monoid-nerd               \
     ttf-mononoki-nerd             \
     ttf-nerd-fonts-symbols        \
     ttf-nerd-fonts-symbols-common \
     ttf-nerd-fonts-symbols-mono   \
     ttf-noto-nerd                 \
     ttf-sourcecodepro-nerd        \
     ttf-terminus-nerd

sudo pacman -S --needed \
     alsa-card-profiles \
     alsa-firmware      \
     alsa-lib           \
     alsa-plugins       \
     alsa-utils

sudo pacman -S --needed \
     a52dec             \
     aom                \
     dav1d              \
     faac               \
     faad2              \
     fdkaac             \
     flac               \
     lame               \
     libao              \
     libavif            \
     libdca             \
     libde265           \
     libdv              \
     libfdk-aac         \
     libheif            \
     libldac            \
     libmad             \
     libmp4v2           \
     libmpcdec          \
     libmpeg2           \
     libogg             \
     libtheora          \
     libvorbis          \
     libvpx             \
     libxv              \
     ogmtools           \
     opencore-amr       \
     opus               \
     portaudio          \
     rav1e              \
     rtmidi             \
     schroedinger       \
     sox                \
     speex              \
     speexdsp           \
     svt-av1            \
     wavpack            \
     x264               \
     x265               \
     xvidcore

sudo pacman -S --needed    \
     gst-devtools          \
     gst-editing-services  \
     gst-libav             \
     gst-plugin-gtk        \
     gst-plugin-msdk       \
     gst-plugin-opencv     \
     gst-plugin-qmlgl      \
     gst-plugin-va         \
     gst-plugin-wpe        \
     gst-plugins-bad       \
     gst-plugins-bad-libs  \
     gst-plugins-base      \
     gst-plugins-base-libs \
     gst-plugins-good      \
     gst-plugins-ugly      \
     gst-rtsp-server       \
     gstreamer             \
     gstreamer-vaapi

sudo pacman -S --needed qjackctl

sudo pacman -S --needed \
     bluez              \
     bluez-cups         \
     bluez-libs         \
     bluez-utils

sudo pacman -S --needed fwupd

sudo pacman -S --needed \
     cups               \
     cups-pdf           \
     cups-pk-helper     \
     gutenprint         \
     pappl              \
     system-config-printer

sudo pacman -S --needed          \
     foomatic-db                 \
     foomatic-db-engine          \
     foomatic-db-gutenprint-ppds \
     foomatic-db-nonfree         \
     foomatic-db-nonfree-ppds    \
     foomatic-db-ppds

sudo pacman -S --needed sane

if [ "$video_card_type" = "AMD" ]
then
  sudo pacman -S --needed \
       vulkan-radeon      \
       xf86-video-amdgpu
fi

if [ "$video_card_type" = "INTEL" ]
then
  sudo pacman -S --needed vulkan-intel
fi

if [ "$video_card_type" = "NVIDIA" ]
then
  sudo pacman -S --needed \
       cuda               \
       cuda-tools         \
       egl-wayland        \
       libvdpau           \
       nvidia-lts         \
       nvidia-settings    \
       nvidia-utils       \
       opencl-nvidia

  echo "options nvidia-drm modeset=1" | sudo tee /etc/modprobe.d/nvidia.conf
fi

if [ "$video_card_type" = "VM" ]
then
  sudo pacman -S --needed \
       xf86-video-fbdev   \
       xf86-video-vesa
fi

sudo pacman -S --needed nvtop

sudo pacman -S --needed \
     mesa               \
     mesa-utils

sudo pacman -S --needed  \
     libclc              \
     ocl-icd             \
     opencl-clhpp        \
     opencl-clover-mesa  \
     opencl-headers      \
     opencl-rusticl-mesa \
     pocl

sudo pacman -S --needed       \
     vulkan-extra-layers      \
     vulkan-extra-tools       \
     vulkan-headers           \
     vulkan-html-docs         \
     vulkan-icd-loader        \
     vulkan-mesa-layers       \
     vulkan-swrast            \
     vulkan-tools             \
     vulkan-utility-libraries \
     vulkan-validation-layers \
     vulkan-virtio

sudo pacman -S --needed cmake

sudo pacman -S --needed gc

sudo pacman -S --needed \
     gcc                \
     gcc-fortran

sudo pacman -S --needed gdb

sudo pacman -S --needed debuginfod

sudo pacman -S --needed make

sudo pacman -S --needed lcov

sudo pacman -S --needed libgccjit

sudo pacman -S --needed go

sudo pacman -S --needed delve

sudo pacman -S --needed goreleaser

sudo pacman -S --needed \
     go-tools           \
     gopls

sudo pacman -S --needed revive

sudo pacman -S --needed staticcheck

sudo pacman -S --needed llvm

sudo pacman -S --needed bear

sudo pacman -S --needed clang

sudo pacman -S --needed compiler-rt

sudo pacman -S --needed \
     libc++             \
     libc++abi

sudo pacman -S --needed lld

sudo pacman -S --needed lldb

sudo pacman -S --needed mold

sudo pacman -S --needed openmp

sudo pacman -S --needed polly

sudo pacman -S --needed musl

sudo pacman -S --needed \
     jdk-openjdk        \
     openjdk-doc        \
     openjdk-src

sudo pacman -S --needed \
     perl               \
     perl-tk

sudo pacman -S --needed pkgconf

sudo pacman -S --needed rustup
rustup set profile default
rustup default stable

rustup component add cargo
rustup component add clippy
rustup component add llvm-tools
rustup component add rust-analyzer
rustup component add rust-docs
rustup component add rust-src
rustup component add rust-std
rustup component add rustc
rustup component add rustfmt

sudo pacman -S --needed \
     wayland            \
     wayland-protocols

sudo pacman -S --needed wl-clipboard

sudo pacman -S --needed libinput

sudo pacman -S --needed \
     xorg               \
     xorg-apps

sudo pacman -S --needed \
     xclip              \
     xsel

for i in {1..5}
do
  clear
  echo "=============================="
  echo "$i KDE Plasma installation ..."
  echo "=============================="
  sudo pacman -S --needed       \
       plasma-integration       \
       plasma-meta              \
       plasma-wayland-protocols \
       plasma-workspace
  sleep 5 && clear
done

sudo pacman -S --needed \
     ark                \
     dolphin            \
     dolphin-plugins    \
     elisa              \
     filelight          \
     gwenview           \
     kamera             \
     kate               \
     kcolorchooser      \
     kcron              \
     kdeconnect         \
     kdialog            \
     kfind              \
     kid3               \
     konsole            \
     krunner            \
     ksystemlog         \
     okular             \
     partitionmanager   \
     print-manager      \
     skanlite           \
     spectacle

sudo pacman -S --needed \
     breeze             \
     breeze-grub        \
     breeze-gtk         \
     breeze-icons

sudo pacman -S --needed \
     sddm               \
     sddm-kcm

sudo pacman -S --needed   \
     editorconfig-checker \
     editorconfig-core-c

sudo pacman -S --needed ed

sudo pacman -S --needed emacs

sudo pacman -S --needed \
     neovim             \
     python-pynvim

sudo pacman -S --needed \
     tree-sitter        \
     tree-sitter-cli

sudo pacman -S --needed vim

sudo pacman -S --needed apparmor

sudo pacman -S --needed \
     7zip               \
     arj                \
     atool              \
     borg               \
     bzip2              \
     cpio               \
     gzip               \
     lhasa              \
     libarchive         \
     lrzip              \
     lz4                \
     lzip               \
     lzo                \
     lzop               \
     minizip            \
     pigz               \
     sz                 \
     tar                \
     unace              \
     unarchiver         \
     unarj              \
     unrar              \
     unzip              \
     xz                 \
     zip                \
     zlib               \
     zstd

git clone https://aur.archlinux.org/paru.git /tmp/paru
cd /tmp/paru      || return
makepkg -si && cd || return
paru -c  --noconfirm
paru -Sc --noconfirm

sudo pacman -S --needed bat

sudo pacman -S --needed \
     cockpit            \
     cockpit-machines   \
     cockpit-packagekit \
     cockpit-storaged   \
     pcp

sudo pacman -S --needed entr

sudo pacman -S --needed eza

sudo pacman -S --needed fd

sudo pacman -S --needed firewalld

sudo pacman -S --needed \
     flatpak            \
     flatpak-kcm

flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

sudo pacman -S --needed fzf

sudo pacman -S --needed gawk

sudo pacman -S --needed gdbm

sudo pacman -S --needed groff

sudo pacman -S --needed grep

sudo pacman -S --needed ripgrep

sudo pacman -S --needed ugrep

sudo pacman -S --needed htop

sudo pacman -S --needed hwinfo

sudo pacman -S --needed just

sudo pacman -S --needed lsof

sudo pacman -S --needed mc

sudo pacman -S --needed packagekit

sudo pacman -S --needed \
     poppler            \
     poppler-data

sudo pacman  -S --needed realtime-privileges
sudo usermod -aG realtime "$(whoami)"

sudo pacman -S --needed reflector

sudo pacman -S --needed rsync

sudo pacman -S --needed smartmontools

sudo pacman -S --needed starship

sudo pacman -S --needed \
     binutils           \
     coreutils          \
     dateutils          \
     diffutils          \
     elfutils           \
     findutils          \
     inetutils          \
     iputils            \
     mailutils          \
     moreutils          \
     pax-utils          \
     pciutils           \
     plotutils          \
     psutils            \
     renameutils        \
     sysfsutils         \
     usbutils

sudo pacman -S --needed tealdeer
tldr --update

sudo pacman -S --needed wezterm

sudo pacman -S --needed zoxide

sudo pacman -S --needed \
     firefox            \
     firefox-i18n-en-gb \
     firefox-i18n-en-us \
     firefox-i18n-ru

sudo pacman -S --needed      \
     libreoffice-still       \
     libreoffice-still-en-gb \
     libreoffice-still-ru    \
     libreoffice-still-sdk

sudo pacman -S --needed qalculate-qt

sudo pacman -S --needed enchant

sudo pacman -S --needed \
     aspell             \
     aspell-en          \
     aspell-ru

sudo pacman -S --needed \
     hunspell           \
     hunspell-en_gb     \
     hunspell-en_us     \
     hunspell-ru

sudo pacman -S --needed \
     hyphen             \
     hyphen-en

sudo pacman -S --needed \
     libmythes          \
     mythes-en

sudo pacman -S --needed ispell

sudo pacman -S --needed nuspell

sudo pacman -S --needed     \
     thunderbird            \
     thunderbird-i18n-en-gb \
     thunderbird-i18n-en-us \
     thunderbird-i18n-ru

{
  echo "\$include /etc/inputrc"
  echo "set bell-style                        none"
  echo "set bind-tty-special-chars              on"
  echo "set blink-matching-paren                on"
  echo "set colored-stats                       on"
  echo "set completion-ignore-case              on"
  echo "set completion-prefix-display-length     5"
  echo "set completion-query-items              10"
  echo "set echo-control-characters            off"
  echo "set editing-mode                     emacs"
  echo "set enable-bracketed-paste              on"
  echo "set enable-keypad                       on"
  echo "set expand-tilde                        on"
  echo "set horizontal-scroll-mode              on"
  echo "set input-meta                          on"
  echo "set mark-directories                    on"
  echo "set mark-modified-lines                off"
  echo "set mark-symlinked-directories          on"
  echo "set match-hidden-files                  on"
  echo "set output-meta                         on"
  echo "set show-all-if-ambiguous               on"
  echo "set show-all-if-unmodified              on"
  echo "set visible-stats                       on"
} > "$HOME"/.inputrc

{
  echo "xrdb -merge .Xresources"
  echo "export DESKTOP_SESSION=plasma"
  echo "exec startplasma-x11"
} > "$HOME"/.xinitrc

{
  echo "Xft.antialias: true"
  echo "Xft.autohint:  false"
  echo "Xft.dpi:       96"
  echo "Xft.hinting:   true"
  echo "Xft.hintstyle: hintslight"
  echo "Xft.lcdfilter: lcddefault"
  echo "Xft.rgba:      rgb"
} > "$HOME"/.Xresources

# sudo systemctl enable firewalld.service
sudo sensors-detect --auto
sudo systemctl enable bluetooth.service
sudo systemctl enable cockpit.socket
sudo systemctl enable cups.service
sudo systemctl enable dbus-broker.service
sudo systemctl enable fstrim.timer
sudo systemctl enable haveged
sudo systemctl enable paccache.timer
sudo systemctl enable plocate-updatedb.timer
sudo systemctl enable reflector.service
sudo systemctl enable reflector.timer
sudo systemctl enable rngd
sudo systemctl enable sddm.service
sudo systemctl enable smartd.service
sudo systemctl enable systemd-oomd.service
sudo systemctl enable systemd-timesyncd.service

git config --global init.defaultBranch "main"
git config --global user.email         "karlcorp@mail.ru"
git config --global user.name          "cyberdyne-skynet"
git config --global credential.helper  store

echo "vm.swappiness=10" | sudo tee /etc/sysctl.d/99-swappiness.conf

# https://lwn.net/Articles/572921/
# https://lonesysadmin.net/2013/12/22/better-linux-disk-caching-performance-vm-dirty_ratio/
echo "vm.dirty_background_ratio=5" | sudo tee    /etc/sysctl.d/99-dirty.conf
echo "vm.dirty_ratio=10"           | sudo tee -a /etc/sysctl.d/99-dirty.conf

xdg-user-dirs-update

if [ -d /base ]
then
  sudo chown "$USER":"$USER" /base
  mkdir -p /base/Projects
fi

if [ -d /data ]
then
  sudo chown "$USER":"$USER" /data
  mkdir -p /data/Torrents
  mkdir -p /data/VM
fi

git clone https://gitlab.com/cyberdyne-skynet/dotfiles.git "$HOME"/.dotfiles

mkdir -p "$HOME"/.emacs.d
mkdir -p "$HOME"/.local/share/fonts
mkdir -p "$HOME"/.local/share/icons

ln -fsv "$HOME"/.dotfiles/.bashrc      "$HOME"/.bashrc
ln -fsv "$HOME"/.dotfiles/.vimrc       "$HOME"/.vimrc
ln -fsv "$HOME"/.dotfiles/.wezterm.lua "$HOME"/.wezterm.lua
ln -fsv "$HOME"/.dotfiles/init.el      "$HOME"/.emacs.d/init.el
ln -fsv "$HOME"/.dotfiles/nvim         "$HOME"/.config/nvim
ln -fsv "$HOME"/.dotfiles/packages.el  "$HOME"/.emacs.d/packages.el

sudo pacman -Sy --noconfirm pacman-mirrorlist
sudo pacman -Sy --noconfirm archlinux-keyring
sudo pacman-key --init
sudo pacman-key --populate archlinux

if [ -f /usr/bin/parted ]
then
  IS_GPT_PARTITION_TABLE="$(parted -l | grep gpt)"
  if [ -f /usr/bin/checkupdates ]
  then
    IS_GRUB_UPDATED="$(checkupdates | grep grub)"
  else
    echo "CHECKUPDATES: command not found"
  fi
else
  echo "PARTED: command not found"
fi

sudo pacman   -Syu
sudo pacman   -Rns "$(pacman -Qdqtt)" --noconfirm
sudo pacman   -Scc                    --noconfirm
sudo paccache -rk0

if [ -z "$IS_GPT_PARTITION_TABLE" ] || [ -z "$IS_GRUB_UPDATED" ]
then
  echo "GRUB is up-to-date"
else
  sudo grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=grub
  sudo grub-mkconfig -o /boot/grub/grub.cfg
fi

echo       "User system installation was finished!"
read -r -p "Press 'enter' to reboot..."
sleep 5 && reboot
