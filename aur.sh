#!/bin/bash

paru -S chez-scheme

paru -S clj-kondo-bin

paru -S clojure-lsp-bin

paru -S jigdo-bin

paru -S nekoray-bin

paru -S sing-geoip-db

paru -S sing-geosite-db

paru -S wxmaxima

sleep 5 && clear
echo "WARNING: restore (delete) settings: /etc/pacman.conf under [options]: 'SigLevel = Never'"
