set nocompatible

language messages POSIX

runtime macros/matchit.vim

try
  colorscheme default
  set background=dark
catch
  echo "Color scheme loading error..."
endtry

highlight TabLine     cterm=NONE ctermbg=NONE ctermfg=NONE
highlight TabLineFill cterm=NONE ctermbg=NONE ctermfg=NONE
highlight TabLineSel  cterm=NONE ctermbg=NONE ctermfg=NONE
highlight VertSplit   cterm=NONE ctermbg=NONE ctermfg=NONE

filetype               on
filetype plugin        on
filetype plugin indent on
syntax                 on

set autochdir
set autoindent
set autoread
set autowriteall
set backspace=2
set browsedir=current
set cindent
set clipboard=unnamedplus
set cmdheight=1
set cmdwinheight=5
set confirm
set copyindent
set diffopt+=vertical
set encoding=utf-8
set expandtab
set fileencoding=utf-8
set fileformats=unix,dos,mac
set fillchars=""
set foldcolumn=0
set foldmethod=indent
set helplang=en
set hidden
set history=8192
set hlsearch
set ignorecase
set iminsert=0
set imsearch=0
set incsearch
set infercase
set langmenu=en
set laststatus=0
set lazyredraw
set magic
set mouse=a
set nobackup
set noerrorbells
set nofoldenable
set nojoinspaces
set nonumber
set norelativenumber
set nostartofline
set noswapfile
set novisualbell
set nowrap
set nowrapscan
set nowritebackup
set pastetoggle=<F2>
set path+=**
set pumheight=5
set pyxversion=3
set regexpengine=0
set report=0
set ruler
set scrolloff=5
set secure
set shiftround
set shiftwidth=2
set shortmess=I
set showcmd
set showfulltag
set showmatch
set showmode
set sidescroll=5
set smartcase
set smartindent
set smarttab
set softtabstop=2
set spell
set spelllang=en
set splitbelow
set splitright
set switchbuf=newtab,vsplit,split,useopen,usetab
set t_Co=256
set tabstop=2
set tagcase=followscs
set termencoding=utf-8
set textwidth=100
set title
set ttimeout
set ttimeoutlen=1000
set ttyfast
set ttymouse=sgr
set undodir=$HOME/.vim/undo
set undofile
set undolevels=8192
set virtualedit=block
set wildmenu

scriptencoding utf-8

let mapleader=" "

cnoreabbrev Q q!
cnoreabbrev W w!
cnoreabbrev q q!
cnoreabbrev w w!

inoremap jj <esc>

map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

map <F3> :Lexplore<cr>

nnoremap n nzz
nnoremap N Nzz

nnoremap <C-down>  <C-w>-
nnoremap <C-left>  <C-w>>
nnoremap <C-right> <C-w><
nnoremap <C-up>    <C-w>+

nnoremap <leader>O O<esc>
nnoremap <leader>o o<esc>

nnoremap <leader><space> :nohlsearch<cr>
nnoremap <leader>bn      :bnext<cr>
nnoremap <leader>bp      :bprevious<cr>
nnoremap <leader>e       :edit<space>
nnoremap <leader>n       :tabnext<cr>
nnoremap <leader>p       :tabprevious<cr>
nnoremap <leader>q       :quit!<cr>
nnoremap <leader>r       :terminal<cr>
nnoremap <leader>s       :split<cr>
nnoremap <leader>t       :tabnew<cr>
nnoremap <leader>v       :vsplit<cr>
nnoremap <leader>w       :write!<cr>

for prefix in ["i", "n", "v"]
  execute prefix . "noremap " . "<c-q>" . " <nop>"
  for key in ["<up>", "<down>", "<left>", "<right>"]
    execute prefix . "noremap " . key . " <nop>"
  endfor
endfor

function! FormatFile()
  silent! normal ml
  silent! normal gg=G
  silent! %s/\s\+$//e
  silent! %s#\($\n\s*\)\+\%$##
  silent! normal 'lzz
  silent! delmarks l
endfunction

if has("autocmd")
  augroup BeforeWritingFile
    autocmd!
    autocmd BufWritePre * retab
    autocmd BufWritePre * call FormatFile()
  augroup END

  if executable("clang-format")
    autocmd! FileType c,cc,cpp,h,hh,hpp
          \ setlocal equalprg=clang-format
  endif

  autocmd! FileType make setlocal noexpandtab
  autocmd! BufEnter,FocusGained * silent! checktime
  autocmd! InsertEnter * norm zz
endif
