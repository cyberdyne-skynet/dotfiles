#!/bin/bash

# shellcheck disable=SC1091
[[ $PS1 && -f /usr/share/bash-completion/bash_completion ]] && \
  . /usr/share/bash-completion/bash_completion
# complete -c  man which
# complete -cf sudo

capp () {
  local project_name="${1:?'ERROR! You must specify a PROJECT NAME'}"
  local activate_command="source ./bin/activate"
  if [ -d "$project_name" ]
  then
    cd "$project_name" && eval "$activate_command" && cd src || return
    clear && echo "The project already exists ..." && echo "" && ls -la . && return
  else
    if [ -f /usr/bin/virtualenv ]
    then
      python -m venv "$project_name"
      cd "$project_name" && eval "$activate_command"
      mkdir -p src && cd src || return
      pip install --upgrade pip
      pip install --upgrade black
      pip install --upgrade ruff
      pip install --upgrade 'python-lsp-server[all]'
      rm -rf "$(pip cache dir)"
      git init && git add . && git commit -m "Python project created ..."
      clear && echo "Python project created ..." && echo "" && ls -la && return
    else
      echo "You need to install 'python-virtualenv' package"
    fi
  fi
}

venv () {
  local venv_name="${1:?'ERROR! You must specify a VENV NAME'}"
  local package_name="$2"
  local activate_command="source ./bin/activate"
  if [ -d "$venv_name" ]
  then
    cd "$venv_name" && eval "$activate_command" || return
    clear && echo "Python Virtual Environment already exists ..." && echo "" && ls -la . && return
  else
    if [ -f /usr/bin/virtualenv ]
    then
      python -m venv "$venv_name"
      cd "$venv_name" && eval "$activate_command"
      pip install --upgrade pip
      if [ -n "$package_name" ]
      then
        pip install --upgrade "$package_name"
      fi
      rm -rf "$(pip cache dir)"
      git init && git add . && git commit -m "Python Virtual Environment created ..."
      clear && echo "Python Virtual Environment created ..." && echo "" && ls -la && return
    else
      echo "You need to install 'python-virtualenv' package"
    fi
  fi
}

caspp () {
  local project_name="${1:?'ERROR! You must specify a PROJECT NAME'}"
  local activate_command="source ./bin/activate"
  if [ -d "$project_name" ]
  then
    capp "$project_name"
  else
    capp "$project_name"
    pip install --upgrade \
        jupyter           \
        jupyterlab        \
        manim             \
        matplotlib        \
        nltk              \
        numpy             \
        pandas            \
        pypandoc          \
        scikit-image      \
        scikit-learn      \
        scipy             \
        sympy
    rm -rf "$(pip cache dir)"
    git add . && git commit -m "Scientific Python project created ..."
    clear && echo "Scientific Python project created ..." && echo "" && ls -la && return
  fi
}

function get_debian() {
  local VERSION="${1:?'ERROR! You must specify a VERSION'}"
  local NUMBER_OF_ISO="${2:?'ERROR! You must specify a NUMBER OF ISO'}"
  local NUMBER_OF_UPDATE_ISO="${3:?'ERROR! You must specify a NUMBER OF UPDATE ISO'}"

  local URL="https://cdimage.debian.org/debian-cd/current/amd64/jigdo-dvd/"

  rm -f -r -- *.db *.jigdo *.list *.template *.tmp *.tmpdir *.unpacked

  if command -v jigdo-lite &> /dev/null
  then
    for ((NUMBER = 1; NUMBER <= "${NUMBER_OF_ISO}"; ++NUMBER))
    do
      wget "${URL}debian-${VERSION}-amd64-DVD-${NUMBER}.jigdo"
      wget "${URL}debian-${VERSION}-amd64-DVD-${NUMBER}.template"
    done

    for ((NUMBER = 1; NUMBER <= "${NUMBER_OF_UPDATE_ISO}"; ++NUMBER))
    do
      wget "${URL}debian-update-${VERSION}-amd64-DVD-${NUMBER}.jigdo"
      wget "${URL}debian-update-${VERSION}-amd64-DVD-${NUMBER}.template"
    done
  else
    return
  fi

  jigdo-lite ./*.jigdo
  rm -f -r -- *.db *.jigdo *.template
}

mkcd () {
  local dir_name="${1:?'ERROR! You must specify a DIRECTORY NAME'}"
  mkdir -p "$dir_name" && cd "$dir_name" || return
}

paru_install () {
  git clone https://aur.archlinux.org/paru.git /tmp/paru
  cd /tmp/paru      || return
  makepkg -si && cd || return
  paru -c  --noconfirm
  paru -Sc --noconfirm
}

print () {
  local file_or_dir_name="${1:?'ERROR! You must specify a FILE or DIRECTORY NAME'}"

  print_file () {
    local file_name="$1"
    clear && realpath "$file_name"
    while read -N 1 -r line
    do
      echo -n "$line"
      sleep 0.05
    done < "$file_name" && echo
  }

  if [ -f "$file_or_dir_name" ]
  then
    local file_name="$file_or_dir_name"
    print_file "$file_name"
  else
    for name in "$file_or_dir_name"/*
    do
      if [ -d "$name" ]
      then
        print "$name"
      else
        print_file "$name"
      fi
    done
  fi
}

rank_mirrors () {
  sudo cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.backup
  sudo reflector                       \
       --age 12                        \
       --country CN,IR,RU,US           \
       --latest 5                      \
       --protocol http,https           \
       --save /etc/pacman.d/mirrorlist \
       --sort rate                     \
       --verbose
}

uc () {
  sudo pacman -Sy --noconfirm archlinux-keyring
  sudo pacman -Sy --noconfirm pacman-mirrorlist
  sudo pacman-key --init
  sudo pacman-key --populate archlinux

  if [ -f /usr/bin/parted ]
  then
    local IS_GPT_PARTITION_TABLE
    IS_GPT_PARTITION_TABLE="$(parted -l | grep gpt)"
    if [ -f /usr/bin/checkupdates ]
    then
      local IS_GRUB_UPDATED
      IS_GRUB_UPDATED="$(checkupdates | grep grub)"
    else
      echo "CHECKUPDATES: command not found"
    fi
  else
    echo "PARTED: command not found"
  fi

  echo ""
  echo "+++++++++++++++++++++++++++++"
  echo "List of all foreign packages:"
  echo "+++++++++++++++++++++++++++++"
  pacman -Qm
  echo "+++++++++++++++++++++++++++++"
  echo "" && sleep 5

  sudo pacman   -Syu
  sudo pacman   -Rns "$(pacman -Qdqtt)" --noconfirm
  sudo pacman   -Scc                    --noconfirm
  sudo paccache -rk0

  if [ -z "$IS_GPT_PARTITION_TABLE" ] || [ -z "$IS_GRUB_UPDATED" ]
  then
    echo "GRUB is up-to-date"
  else
    sudo grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=grub
    sudo grub-mkconfig -o /boot/grub/grub.cfg
  fi

  if [ -f /usr/bin/flatpak ]
  then
    flatpak update             -y
    flatpak uninstall --unused -y
  fi

  if [ -f /usr/bin/paru ]
  then
    paru -Sua --noconfirm
    paru -c   --noconfirm
    paru -Sc  --noconfirm
  fi

  if [ -f /usr/bin/rustup ]
  then
    rustup update
  fi

  if [ -f /usr/bin/tldr ]
  then
    tldr --update
  fi
}

PROMPT_DIRTRIM=2

export ALTERNATE_EDITOR=""
export EDITOR="emacs"
export GOPATH="$HOME"/.go
export HISTCONTROL="erasedups:ignorespace"
export HISTIGNORE=".:..:c:h:x:cd:la:ll:ls"
export HISTSIZE=8192
export LANG="en_US.UTF-8"
export LC_ALL="en_US.UTF-8"
export PROMPT_COMMAND="history -a"
export PS1=$'\n\w \U00BB '
export TERM=xterm-256color
export VIRTUAL_ENV_DISABLE_PROMPT=1
export VISUAL="$EDITOR"

shopt -s autocd
shopt -s cdable_vars
shopt -s cdspell
shopt -s checkhash
shopt -s checkjobs
shopt -s checkwinsize
shopt -s cmdhist
shopt -s direxpand
shopt -s dirspell
shopt -s dotglob
shopt -s expand_aliases
shopt -s globstar
shopt -s histappend
shopt -s histreedit
shopt -s histverify

if [ -f /usr/bin/starship ]
then
  eval "$(starship init bash)"
fi

alias ..="cd ..;       pwd"
alias .2="cd ../..;    pwd"
alias .3="cd ../../..; pwd"
alias .="pwd"
alias al="la"
alias bd="cd \$OLDPWD"
alias c="clear && clear"
alias count='find . -type f | wc -l'
alias df="df -Tha --total | column -t"
alias diff="diff -y --color=auto"
alias du="du -ach | sort -h"
alias egrep="egrep --color=auto"
alias fgrep="fgrep --color=auto"
alias free="free -mt"
alias grep="grep --color=auto"
alias h="history"
alias hs="history | grep"
alias ip="ip -color=auto"
alias j="jobs -l"
alias l="less"
alias la="ls -lahi --color=auto"
alias ll="ls -lhi --color=auto"
alias ln="ln -iv"
alias ls="ls -F --color=auto"
alias m="more"
alias me="chmod a+x"
alias mkdir="mkdir -pv"
alias mv="mv -v"
alias ping="ping -c 5"
alias ps="ps auxf"
alias q="exit"
alias reload="reset && source \$HOME/.bashrc && clear"
alias rm="rm -Iv"
alias sl="ls"
alias t="tree"
alias tarc="tar czvf"
alias tart="tar tzvf"
alias x="exit"

if [ -d "$HOME"/.dotfiles ]
then
  alias d="cd \$HOME/.dotfiles"
fi

if [ -d /base/Projects ]
then
  alias p="cd /base/Projects"
fi

if [ -f /usr/bin/bat ]
then
  alias cat="bat"
fi

if [ -f /usr/bin/eza ]
then
  if [ -f /usr/bin/git ]
  then
    alias ls="eza --color=always --git --group-directories-first"
  else
    alias ls="eza --color=always --group-directories-first"
  fi
fi

if [ -f /usr/bin/fzf ]
then
  eval "$(fzf --bash)"
fi

if [ -f /usr/bin/git ]
then
  alias commit="git commit -m"
  alias ga="git add --all"
  alias gd="git diff"
  alias gl="git log"
  alias push="git push origin main"
  alias status="git status"

  if [ -f /usr/bin/lazygit ]
  then
    alias lg="lazygit"
  fi
fi

if [ -f /usr/bin/bc ]
then
  alias bc="clear && bc -lq"
fi

if [ -f /usr/bin/emacs ]
then
  alias cb="emacs \$HOME/.dotfiles/bashrc.org &"
  alias ce="emacs \$HOME/.dotfiles/emacs.org  &"
  alias enw="emacs -nw"

  if [ -z "$INSIDE_EMACS" ]
  then
    set -o emacs
  fi

  e () { emacs "$@" & }

  clean_emacs_directory () {
    if [ -d "$HOME"/.emacs.d ]
    then
      for file_or_dir in "$HOME"/.emacs.d/*
      do
        if [ ! -L "$file_or_dir" ]
        then
          rm -rf "$file_or_dir"
        fi
      done
    fi
  }

fi

if [ -f /usr/bin/magick ]
then
  show () { magick display "$@" & }
fi

if [ -f /usr/bin/nvim ]
then
  alias n="nvim"

  clean_neovim_directory () {
    if [ -d "$HOME"/.local/share/nvim ]
    then
      rm -rf "$HOME"/.cache/nvim
      rm -rf "$HOME"/.local/share/nvim
    fi
  }

fi

if [ -f /usr/bin/vim ]
then
  alias v="vim"
  alias cv="vim \$HOME/.dotfiles/.vimrc"
fi

if [ -f /usr/bin/zoxide ]
then
  eval "$(zoxide init bash)"
  alias cd="z"
fi
